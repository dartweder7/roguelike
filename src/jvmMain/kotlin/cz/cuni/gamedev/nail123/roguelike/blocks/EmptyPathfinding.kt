package cz.cuni.gamedev.nail123.roguelike.blocks

import cz.cuni.gamedev.nail123.roguelike.tiles.GameTiles
import org.hexworks.zircon.api.data.Tile

class EmptyPathfinding: GameBlock(GameTiles.EMPTY) {
    override val blocksMovement = false
    override val blocksVision = true

    override var baseTile: Tile = GameTiles.BLACK
}