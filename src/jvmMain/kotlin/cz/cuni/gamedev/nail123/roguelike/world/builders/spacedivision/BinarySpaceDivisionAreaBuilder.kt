package cz.cuni.gamedev.nail123.roguelike.world.builders.spacedivision

import cz.cuni.gamedev.nail123.roguelike.GameConfig
import cz.cuni.gamedev.nail123.roguelike.blocks.Empty
import cz.cuni.gamedev.nail123.roguelike.blocks.EmptyPathfinding
import cz.cuni.gamedev.nail123.roguelike.blocks.Floor
import cz.cuni.gamedev.nail123.roguelike.blocks.Wall
import cz.cuni.gamedev.nail123.roguelike.entities.objects.Door
import cz.cuni.gamedev.nail123.roguelike.extensions.manhattanDistance
import cz.cuni.gamedev.nail123.roguelike.mechanics.Pathfinding
import cz.cuni.gamedev.nail123.roguelike.world.builders.AreaBuilder
import org.hexworks.zircon.api.data.*
import org.hexworks.zircon.api.data.Position
import kotlin.random.Random

class BinarySpaceDivisionAreaBuilder(size: Size3D, visibleSize: Size3D = GameConfig.VISIBLE_SIZE) : AreaBuilder(size, visibleSize) {

    val mapChars: MutableList<MutableList<Char>> = MutableList(width) { MutableList(height) {'0'} }
    // Rect - the room, MutableList<Position> - positions for doors, MutableList<Boolean> - whether this door was actually selected
    val rooms: MutableList<Triple<Rect, MutableList<Position>, MutableList<Boolean>>> = mutableListOf()
    var globalRect: Rect = Rect.create(Position.defaultPosition(), Size.one())
    var smallerGlobalRect: Rect = Rect.create(Position.defaultPosition(), Size.one())
    val NUMBER_ITERATIONS = 5
    val MIN_SIDE_CONTAINER = 7
    val EMPTY_BLOCK_SYMBOL = '0'

    init {
        for (i in 0 until width) {
            for (j in 0 until height) {
                mapChars[i][j] = EMPTY_BLOCK_SYMBOL
            }
        }
    }

    fun charToBlock(char: Char) = when(char) {
        '#' -> Wall()
        'D' -> Floor().apply { entities.add(Door()) }
        EMPTY_BLOCK_SYMBOL -> Empty()
        else -> Floor()
    }

    fun charToBlockPathfinding(char: Char) = when(char) {
        '#' -> Wall()
        'D' -> Floor().apply { entities.add(Door()) }
        EMPTY_BLOCK_SYMBOL -> EmptyPathfinding()
        else -> Floor()
    }

    override fun create() = apply {
        val firstIterationBlock = Rect.create(Position.create(0,0), Size.create(width, height))
        globalRect = firstIterationBlock
        smallerGlobalRect = firstIterationBlock
            .withPosition(Position.create(4, 4))
            .withSize(Size.create(firstIterationBlock.width - 8, firstIterationBlock.height - 8))
        val containerTree = splitContainer(firstIterationBlock, NUMBER_ITERATIONS)
        outputNode(containerTree)

        generateRoomExits()

        // Convert from chars to game objects to enable pathfinding
        for (x in 0 until width) {
            for (y in 0 until height) {
                val char = mapChars[x][y]
                blocks[Position3D.create(x, y, 0)] = charToBlockPathfinding(char)
            }
        }

        // doors
        val doorPaths = mutableListOf<List<Position3D>>()
        rooms.map {
            val otherDoors = mutableListOf<Position>()
            rooms.filter { otherRoom -> otherRoom != it }.forEach { otherRoom -> otherDoors.addAll(otherRoom.second)}
            it.second.map { door: Position ->
                val doorTo = otherDoors.sortedBy {
                        otherDoor -> (door.to3DPosition(0) - otherDoor.to3DPosition(0)).manhattanDistance
                }.first()
                val indexOfDoorFrom = it.second.indexOf(door)
                var otherRoom = rooms.first { roomTo -> roomTo.second.contains(doorTo) }
                val indexOfDoorTo = otherRoom.second.indexOf(doorTo)
                if ((it.third[indexOfDoorFrom] == false)) {
                    val path = Pathfinding.aStar(door.toPosition3D(0), doorTo.toPosition3D(0), this, Pathfinding.fourDirectional, heuristic = Pathfinding.manhattan)
                    if (path != null) {
                        doorPaths.add(path.path)
                        it.third[indexOfDoorFrom] = true
                        otherRoom.third[indexOfDoorTo] = true
                    }
                }
        } }
        // rooms which were not connected in the first pass
        rooms.filter { room ->
            room.third.find { true } == null
        }.map { room: Triple<Rect, MutableList<Position>, MutableList<Boolean>> ->
            val otherDoors = mutableListOf<Position>()
            rooms.filter { otherRoom -> otherRoom != room }.forEach { otherRoom -> otherDoors.addAll(otherRoom.second)}
            if (room.second.isNotEmpty()) {
                val door: Position = room.second.random()
                val doorTo = otherDoors.sortedBy {
                        otherDoor -> (door.to3DPosition(0) - otherDoor.to3DPosition(0)).manhattanDistance
                }.first()
                val path = Pathfinding.aStar(door.toPosition3D(0), doorTo.toPosition3D(0), this, Pathfinding.fourDirectional, heuristic = Pathfinding.manhattan)
                if (path != null) {
                    doorPaths.add(path.path)
                }
            }
        }

        // Add Corridors (e.g. build a wall KKona)
        doorPaths.forEach{ path ->
            path.forEach { pos3d ->
                val position = pos3d.to2DPosition()
                if (mapChars[position.x][position.y] != 'D') {
                    mapChars[position.x][position.y] = '.'
                    putWallsAround(position, true)
                }

            }
        }

        // Add Doors
        rooms.forEach { room ->
            room.third.forEachIndexed { index, isAdded ->
                if (isAdded) {
                    val pos = room.second[index]
                    mapChars[pos.x][pos.y] = 'D'
                }
            }
        }

        for (x in 0 until width) {
            for (y in 0 until height) {
                val char = mapChars[x][y]
                blocks[Position3D.create(x, y, 0)] = charToBlock(char)
            }
        }
    }

    fun generateRoomExits() {
        // copypasta (mmm pasta homer.jpg) code
        // rooms receive up to 3 doors based on the amount of corners which pass the inside check of smallerGlobalRect
        for (it in rooms) {
            var cornersInside = 0
            if (smallerGlobalRect.containsPosition(it.first.topLeft))
                cornersInside += 1
            if (smallerGlobalRect.containsPosition(it.first.topRight))
                cornersInside += 1
            if (smallerGlobalRect.containsPosition(it.first.bottomRight))
                cornersInside += 1
            if (smallerGlobalRect.containsPosition(it.first.bottomLeft))
                cornersInside += 1
            when (cornersInside) {
                1 -> {
                    // 2 entrances
                    var door1: Position = Position.zero()
                    var door2: Position = Position.zero()
                    if (smallerGlobalRect.containsPosition(it.first.topLeft)) {
                        val validPositionsLeftSide = mutableListOf<Position>()
                        for (i in 1 until (it.first.bottomLeft.y - it.first.topLeft.y - 1)) {
                            validPositionsLeftSide.add(it.first.topLeft.withRelativeY(i))
                        }
                        val validPositionsTopSide = mutableListOf<Position>()
                        for (i in 1 until (it.first.topRight.x - it.first.topLeft.x - 1)) {
                            validPositionsTopSide.add(it.first.topLeft.withRelativeX(i))
                        }
                        door1 = validPositionsLeftSide.random()
                        door2 = validPositionsTopSide.random()
                    } else if (smallerGlobalRect.containsPosition(it.first.topRight)) {
                        val validPositionsTopSide = mutableListOf<Position>()
                        for (i in 1 until (it.first.topRight.x - it.first.topLeft.x - 1)) {
                            validPositionsTopSide.add(it.first.topLeft.withRelativeX(i))
                        }
                        val validPositionsRightSide = mutableListOf<Position>()
                        for (i in 1 until (it.first.bottomRight.y - it.first.topRight.y - 1)) {
                            // for some reason i have to subtract 1 from X here and in other places
                            validPositionsRightSide.add(it.first.topRight.withRelativeX(-1).withRelativeY(i))
                        }
                        door1 = validPositionsTopSide.random()
                        door2 = validPositionsRightSide.random()
                    } else if (smallerGlobalRect.containsPosition(it.first.bottomRight)) {
                        val validPositionsRightSide = mutableListOf<Position>()
                        for (i in 1 until (it.first.bottomRight.y - it.first.topRight.y - 1)) {
                            validPositionsRightSide.add(it.first.topRight.withRelativeX(-1).withRelativeY(i))
                        }
                        val validPositionsBottomSide = mutableListOf<Position>()
                        for (i in 1 until (it.first.bottomRight.x - it.first.bottomLeft.x - 1)) {
                            validPositionsBottomSide.add(it.first.bottomLeft.withRelativeY(-1).withRelativeX(i))
                        }
                        door1 = validPositionsRightSide.random()
                        door2 = validPositionsBottomSide.random()
                    } else if (smallerGlobalRect.containsPosition(it.first.bottomLeft)) {
                        val validPositionsBottomSide = mutableListOf<Position>()
                        for (i in 1 until (it.first.bottomRight.x - it.first.bottomLeft.x - 1)) {
                            validPositionsBottomSide.add(it.first.bottomLeft.withRelativeY(-1).withRelativeX(i))
                        }
                        val validPositionsLeftSide = mutableListOf<Position>()
                        for (i in 1 until (it.first.bottomLeft.y - it.first.topLeft.y - 1)) {
                            validPositionsLeftSide.add(it.first.topLeft.withRelativeY(i))
                        }
                        door1 = validPositionsBottomSide.random()
                        door2 = validPositionsLeftSide.random()
                    }
                    if (door1 == Position.zero() && door2 == Position.zero())
                        continue
                    it.second.add(door1)
                    it.second.add(door2)
                    it.third.add(false)
                    it.third.add(false)
                    // to instantly show the doors
//                    mapChars[door1.x][door1.y] = 'D'
//                    mapChars[door2.x][door2.y] = 'D'
                }
                2 -> {
                    var door1: Position = Position.zero()
                    var door2: Position = Position.zero()
                    var door3: Position = Position.zero()
                    if (smallerGlobalRect.containsPosition(it.first.topLeft) &&
                        smallerGlobalRect.containsPosition(it.first.topRight)) {
                        // left
                        // top
                        // right
                        val validPositionsLeftSide = mutableListOf<Position>()
                        for (i in 1 until (it.first.bottomLeft.y - it.first.topLeft.y - 1)) {
                            validPositionsLeftSide.add(it.first.topLeft.withRelativeY(i))
                        }
                        val validPositionsTopSide = mutableListOf<Position>()
                        for (i in 1 until (it.first.topRight.x - it.first.topLeft.x - 1)) {
                            validPositionsTopSide.add(it.first.topLeft.withRelativeX(i))
                        }
                        val validPositionsRightSide = mutableListOf<Position>()
                        for (i in 1 until (it.first.bottomRight.y - it.first.topRight.y - 1)) {
                            validPositionsRightSide.add(it.first.topRight.withRelativeX(-1).withRelativeY(i))
                        }
                        door1 = validPositionsLeftSide.random()
                        door2 = validPositionsTopSide.random()
                        door3 = validPositionsRightSide.random()
                    } else if (smallerGlobalRect.containsPosition(it.first.topRight) &&
                            smallerGlobalRect.containsPosition(it.first.bottomRight)) {
                        // top
                        // right
                        // bottom
                        val validPositionsTopSide = mutableListOf<Position>()
                        for (i in 1 until (it.first.topRight.x - it.first.topLeft.x - 1)) {
                            validPositionsTopSide.add(it.first.topLeft.withRelativeX(i))
                        }
                        val validPositionsRightSide = mutableListOf<Position>()
                        for (i in 1 until (it.first.bottomRight.y - it.first.topRight.y - 1)) {
                            validPositionsRightSide.add(it.first.topRight.withRelativeX(-1).withRelativeY(i))
                        }
                        val validPositionsBottomSide = mutableListOf<Position>()
                        for (i in 1 until (it.first.bottomRight.x - it.first.bottomLeft.x - 1)) {
                            validPositionsBottomSide.add(it.first.bottomLeft.withRelativeY(-1).withRelativeX(i))
                        }
                        door1 = validPositionsTopSide.random()
                        door2 = validPositionsRightSide.random()
                        door3 = validPositionsBottomSide.random()
                    } else if (smallerGlobalRect.containsPosition(it.first.bottomRight) &&
                        smallerGlobalRect.containsPosition(it.first.bottomLeft)) {
                        // right
                        // bottom
                        // left
                        val validPositionsRightSide = mutableListOf<Position>()
                        for (i in 1 until (it.first.bottomRight.y - it.first.topRight.y - 1)) {
                            validPositionsRightSide.add(it.first.topRight.withRelativeX(-1).withRelativeY(i))
                        }
                        val validPositionsBottomSide = mutableListOf<Position>()
                        for (i in 1 until (it.first.bottomRight.x - it.first.bottomLeft.x - 1)) {
                            validPositionsBottomSide.add(it.first.bottomLeft.withRelativeY(-1).withRelativeX(i))
                        }
                        val validPositionsLeftSide = mutableListOf<Position>()
                        for (i in 1 until (it.first.bottomLeft.y - it.first.topLeft.y - 1)) {
                            validPositionsLeftSide.add(it.first.topLeft.withRelativeY(i))
                        }
                        door1 = validPositionsRightSide.random()
                        door2 = validPositionsBottomSide.random()
                        door3 = validPositionsLeftSide.random()
                    } else if (smallerGlobalRect.containsPosition(it.first.bottomLeft) &&
                        smallerGlobalRect.containsPosition(it.first.topLeft)) {
                        // bottom
                        // left
                        // top
                        val validPositionsBottomSide = mutableListOf<Position>()
                        for (i in 1 until (it.first.bottomRight.x - it.first.bottomLeft.x - 1)) {
                            validPositionsBottomSide.add(it.first.bottomLeft.withRelativeY(-1).withRelativeX(i))
                        }
                        val validPositionsLeftSide = mutableListOf<Position>()
                        for (i in 1 until (it.first.bottomLeft.y - it.first.topLeft.y - 1)) {
                            validPositionsLeftSide.add(it.first.topLeft.withRelativeY(i))
                        }
                        val validPositionsTopSide = mutableListOf<Position>()
                        for (i in 1 until (it.first.topRight.x - it.first.topLeft.x - 1)) {
                            validPositionsTopSide.add(it.first.topLeft.withRelativeX(i))
                        }
                        door1 = validPositionsBottomSide.random()
                        door2 = validPositionsLeftSide.random()
                        door3 = validPositionsTopSide.random()
                    }
                    if (door1 == Position.zero() && door2 == Position.zero() && door3 == Position.zero())
                        continue
                    it.second.add(door1)
                    it.second.add(door2)
                    it.second.add(door3)
                    it.third.add(false)
                    it.third.add(false)
                    it.third.add(false)
                    // to instantly show the doors
//                    mapChars[door1.x][door1.y] = 'D'
//                    mapChars[door2.x][door2.y] = 'D'
//                    mapChars[door3.x][door3.y] = 'D'
                }
                4 -> {
                    // 3 entrances
                    var door1: Position
                    var door2: Position
                    var door3: Position
                    var door4: Position

                    val validPositionsLeftSide = mutableListOf<Position>()
                    for (i in 1 until (it.first.bottomLeft.y - it.first.topLeft.y - 1)) {
                        validPositionsLeftSide.add(it.first.topLeft.withRelativeY(i))
                    }
                    val validPositionsTopSide = mutableListOf<Position>()
                    for (i in 1 until (it.first.topRight.x - it.first.topLeft.x - 1)) {
                        validPositionsTopSide.add(it.first.topLeft.withRelativeX(i))
                    }
                    val validPositionsRightSide = mutableListOf<Position>()
                    for (i in 1 until (it.first.bottomRight.y - it.first.topRight.y - 1)) {
                        validPositionsRightSide.add(it.first.topRight.withRelativeX(-1).withRelativeY(i))
                    }
                    val validPositionsBottomSide = mutableListOf<Position>()
                    for (i in 1 until (it.first.bottomRight.x - it.first.bottomLeft.x - 1)) {
                        validPositionsBottomSide.add(it.first.bottomLeft.withRelativeY(-1).withRelativeX(i))
                    }
                    door1 = validPositionsLeftSide.random()
                    door2 = validPositionsTopSide.random()
                    door3 = validPositionsRightSide.random()
                    door4 = validPositionsBottomSide.random()
                    val selectedDoors = mutableListOf<Position>(door1, door2, door3, door4)
                    selectedDoors.shuffle()
                    selectedDoors.removeFirst()

                    if (door1 == Position.zero() && door2 == Position.zero() && door3 == Position.zero())
                        continue
                    it.second.addAll(selectedDoors)
                    it.third.add(false)
                    it.third.add(false)
                    it.third.add(false)
                    // to instantly show the doors
//                    mapChars[selectedDoors[0].x][selectedDoors[0].y] = 'D'
//                    mapChars[selectedDoors[1].x][selectedDoors[1].y] = 'D'
//                    mapChars[selectedDoors[2].x][selectedDoors[2].y] = 'D'
                }
            }
        }
    }

    fun putWallsAround(position: Position, diagonal: Boolean) {
        val positionUp = position.withRelativeY(-1)
        val positionRight = position.withRelativeX(1)
        val positionDown = position.withRelativeY(1)
        val positionLeft = position.withRelativeX(-1)
        val positions: MutableList<Position> = mutableListOf(positionUp, positionRight, positionDown, positionLeft)
        if (diagonal) {
            val positionTopRight = position.withRelativeX(1).withRelativeY(-1)
            val positionBottomRight = position.withRelativeX(1).withRelativeY(1)
            val positionBottomLeft = position.withRelativeX(-1).withRelativeY(1)
            val positionTopLeft = position.withRelativeX(-1).withRelativeY(-1)
            positions.addAll(listOf(positionTopRight, positionBottomRight, positionBottomLeft, positionTopLeft))
        }
        positions.forEach {
            if (globalRect.containsPosition(it)) {
                if (mapChars[it.x][it.y] == EMPTY_BLOCK_SYMBOL)
                    mapChars[it.x][it.y] = '#'
            }
        }
    }

    // recursively, output leaves of our BSP tree
    fun outputNode(node: Node?) {
        if (node == null)
            return
        if (node.leftChild != null) {
            node.leftChild!!.room = createRandomRoom(node.leftChild!!.containerRect)
            if (node.leftChild!!.room!!.width < 4 || node.leftChild!!.room!!.height < 4) {
                println("small room detected")
                node.leftChild = null
                return
            }
            outputNode(node.leftChild)
        }
        if (node.rightChild != null) {
            node.rightChild!!.room = createRandomRoom(node.rightChild!!.containerRect)
            if (node.rightChild!!.room!!.width < 4 || node.rightChild!!.room!!.height < 4) {
                node.rightChild = null
                println("small room detected")
                return
            }
            outputNode(node.rightChild)
        }
        if (node.leftChild == null && node.rightChild == null) {
            rooms.add(Triple(node.room!!, mutableListOf(), mutableListOf()))
            addRoomToOutput(node.room!!)
        }

    }

    fun splitContainer(container: Rect, iterations: Int): Node {
        val root = Node(container, null, null)
        if (iterations != 0) {
            if (container.size.width < MIN_SIDE_CONTAINER * 2 || container.size.height < MIN_SIDE_CONTAINER * 2)
                return root
            val splitContainers = randomSplit(container)
            root.leftChild = splitContainer(splitContainers.first, iterations - 1)
            root.rightChild = splitContainer(splitContainers.second, iterations - 1)
        }
        return root
    }

    fun createRandomRoom(sourceRect: Rect): Rect {
        if (sourceRect.width <= 5 || sourceRect.height <= 5)
            println("possibly degenerate rect: ${sourceRect.position} ${sourceRect.size}")
        val offsetChangeX = Random.nextInt(1,3)
        val offsetChangeY = Random.nextInt(1, 3)
        val sizeChangeX = offsetChangeX * 2
        val sizeChangeY = offsetChangeY * 2
        return sourceRect
            .withPosition(Position.create(sourceRect.x + offsetChangeX, sourceRect.y + offsetChangeX))
            .withSize(Size.create(sourceRect.width - sizeChangeX, sourceRect.height - sizeChangeY))
    }

    fun addRoomToOutput(roomRect: Rect) {
        if (roomRect.width < 4 || roomRect.size.height < 4) {
            println("small room detected on output")
            return
        }
        for (row in 0 until width) {
            for (column in 0 until height) {
                if (roomRect.containsPosition(Position.create(row, column))) {
                    if ((row == roomRect.x) || (row == roomRect.topRight.x - 1) || (column == roomRect.y) || (column == roomRect.bottomRight.y - 1)) {
                        mapChars[row][column] = '#'
                        continue
                    }
                    mapChars[row][column] = '.'
                }
            }
        }
    }

    fun randomSplit(rectToSplit: Rect): Pair<Rect, Rect> {
        val minLength = 5
//        val maxLength = 8
        val ASPECT_RATIO_W = 0.35
        val ASPECT_RATIO_H = 0.45

        if (Random.nextBoolean()) {
            var randomHeight = Random.nextInt(minLength, rectToSplit.height)
            var splitRects = rectToSplit.splitVertical(randomHeight)

            if ((splitRects.first.size.height.toFloat() / splitRects.first.size.width.toFloat()) < ASPECT_RATIO_W ||
                    splitRects.second.size.height.toFloat() / splitRects.second.size.width.toFloat() < ASPECT_RATIO_W) {
                splitRects = randomSplit(rectToSplit)
            }
            return splitRects
        }
        else {
            var randomWidth = Random.nextInt(minLength, rectToSplit.width)
            var splitRects = rectToSplit.splitHorizontal(randomWidth)

            if (splitRects.first.size.width.toFloat() / splitRects.first.size.height.toFloat() < ASPECT_RATIO_H ||
                splitRects.second.size.width.toFloat() / splitRects.second.size.height.toFloat() < ASPECT_RATIO_H) {
                splitRects = randomSplit(rectToSplit)
            }
            return splitRects
        }
    }
}

class Node(
    val containerRect: Rect,
    var leftChild: Node?,
    var rightChild: Node?,
    var room: Rect? = null
) {
    fun getNodes(): Collection<Node> {
        if (this.leftChild == null && this.rightChild == null) {
            return listOf(this)
        }
        val leftList = leftChild?.getNodes() ?: emptyList()
        val rightList = rightChild?.getNodes() ?: emptyList()
        return leftList.plus(rightList)
    }

    fun getLevel(level: Int): Sequence<Node> {
        val queue = mutableListOf<Node>()
        if (level == 1) {
            queue.add(this)
            return queue.asSequence()
        }
        if (this.leftChild != null) {
            queue.addAll(this.leftChild!!.getLevel(level - 1))
        }
        if (this.rightChild != null) {
            queue.addAll(this.rightChild!!.getLevel(level - 1))
        }
        return queue.asSequence()
    }
}