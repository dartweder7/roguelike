package cz.cuni.gamedev.nail123.roguelike.world.builders.wavefunctioncollapse

import cz.cuni.gamedev.nail123.roguelike.GameConfig
import cz.cuni.gamedev.nail123.roguelike.blocks.Floor
import cz.cuni.gamedev.nail123.roguelike.blocks.Wall
import cz.cuni.gamedev.nail123.roguelike.entities.objects.Door
import cz.cuni.gamedev.nail123.roguelike.extensions.toIntArray2D
import cz.cuni.gamedev.nail123.roguelike.mechanics.Pathfinding
import cz.cuni.gamedev.nail123.roguelike.world.builders.AreaBuilder
import org.hexworks.zircon.api.data.Position3D
import org.hexworks.zircon.api.data.Size3D
import org.mifek.wfc.datastructures.IntArray2D
import org.mifek.wfc.models.OverlappingCartesian2DModel
import org.mifek.wfc.models.options.Cartesian2DModelOptions
import java.io.File

class WFCAreaBuilder(size: Size3D, visibleSize: Size3D = GameConfig.VISIBLE_SIZE) : AreaBuilder(size, visibleSize) {
    val sourceArray2D = File("src/jvmMain/resources/levels/wfc_sample.txt")
        .readLines()
        .toIntArray2D()

    fun charToBlock(char: Char) = when(char) {
        '#' -> Wall()
        'D' -> Floor().apply { entities.add(Door()) }
        else -> Floor()
    }
    // 62 tiles width, 42 tiles height, each tile is 16x16 px
    // the model has 2400 waves
    // 62*42 = 2604

    override fun create() = apply {
        println("======================================================================")
        println("looking at input")
        println("# of items: " + sourceArray2D.size)
        println("1 single item: " + sourceArray2D[0])
        val inputElements = sourceArray2D.asIntArray().toSet()
        println("input elements: $inputElements")
        val model = OverlappingCartesian2DModel(
            input = sourceArray2D,
            overlap = 2,
            outputWidth = width,
            outputHeight = height,
            options = Cartesian2DModelOptions(
                periodicInput = true
            )
        )

        val output = getOutputFrom(model)

        // place player randomly to be able to flood test
        this.addAtEmptyPosition(
            this.player,
            Position3D.create(0, 0, 0),
            GameConfig.VISIBLE_SIZE
        )
        println("our width: "+ width)

        // have to construct the level first so that Pathfinding works
        for (x in 0 until width) {
            for (y in 0 until height) {
                val char = output[x, y].toChar()
                blocks[Position3D.create(x, y, 0)] = charToBlock(char)
            }
        }

        // if the next wall is in 1 pixel, just expand the whole side
        // if the next wall is in >=2 pixels, make a corridor
        for (x in 0 until width) {
            for (y in 0 until height) {
                val floodFill = Pathfinding.floodFill(Position3D.create(x, y, 0), this)
                println(floodFill.keys.size)
            }
        }
        // remove player when done
        this.removeEntity(this.player)
    }

    fun getOutputFrom(model: OverlappingCartesian2DModel): IntArray2D {
        val maxAttempts = 10
        val algorithm = model.build()

        for (i in 1..maxAttempts) {
            val success = algorithm.run()
            if (success) break
            if (i == maxAttempts) throw Exception("Wave Function Collapse algorithm didn't find solution!")
        }

        return model.constructOutput(algorithm)
    }
}